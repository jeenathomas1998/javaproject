package proj;
import java.time.LocalDate;		
import java.util.Scanner;

//menu class
public class Menu {
	

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			Scanner scanner=new Scanner(System.in);
			int ch;
			FruitService fruitService=new FruitServiceImpl();
			do{
				System.out.println("1.Add\n2.Display\n3.Search\n4.Sort\n5.SearchDate\n6.download\n7.Exit\nEnter the choice:");
				ch=scanner.nextInt();
				switch(ch) {
				
//get the input from the user and handling the exception
				case 1:
					try {
					System.out.println("enter the Fruit name:");
					String n=scanner.next();
					System.out.println("enter the price");
					int p=scanner.nextInt();
					System.out.println("Enter the Expiry date :");
					String expirydate=scanner.next();
					LocalDate d=LocalDate.parse(expirydate);
                    System.out.println("Enter the type of fruit");
					String  value=scanner.next();
					Type type=Type.valueOf(value.toUpperCase());
					Fruit fruits=new Fruit(n,p,d,type);
					fruitService.add(fruits);
				
					}catch(Exception exception) {
						System.out.println("Exception");
					}
					break;
//					display the input taken from the user
				case 2:
					 fruitService.display();
					break;
				case 3:
//					Search the elements
					System.out.println("serach element:");
					String name=scanner.next();
					fruitService.search(name);
					break;
				case 4:
//					sort 
					fruitService.sort();
					break;
				case 5:
//					Search fruits based on date
					System.out.println("Enter the Expiry date:");
					String expirydate=scanner.next();
					LocalDate d=LocalDate.parse(expirydate);
					fruitService.searchDate(d);
				case 6:
					   fruitService.download();
						break;
				case 7:
				System.out.println("You have Exited...");
				System.exit(ch);
				default:
				System.out.println("Enter Valid option to proceed!!!");
				}
			}while(ch!=7);
		
			scanner.close();

		} 

	}
