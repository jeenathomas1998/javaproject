package proj;
import java.time.LocalDate;	
import java.util.Objects;


public class Fruit implements Comparable<Fruit>  {

//	Declaring variables
	private String name;
	private int price=0;
	private LocalDate expirydate;
	private Type type ;
	

	public Fruit(String name,int price,LocalDate expirydate,Type type) {
		this.name=name;
		this.price=price;
		this.expirydate=expirydate;
		this.type=type;
	}
	
//	generating the getters and setters 
	public String getname() {
		return name;
	}
	public void setname(String name) {
		this.name = name;
	}
	
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public LocalDate getDate() {
		return expirydate;
		
	}

	public void setDate(LocalDate expirydate) {
		this.expirydate = expirydate;
	}

	public Enum<Type> getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	//toString method and StringBuffer/StringBuilder method
	public String toString()
	{	
		return new StringBuffer().append(" Name:").append(name).append("\n").append("Price:").append(price).append("\n").append("Date:").append(expirydate).append("Type:").append(type).append("\n").toString();
//		return new StringBuilder().append(Name).append("\n").append(brandName).append("\n").append(price).append("\n").append(Date).toString();
		
	}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Fruit other = (Fruit) obj;
			return Objects.equals(expirydate, other.expirydate) && Objects.equals(name, other.name) && Objects.equals(type, other.type)
					&& price == other.price;
		}


	
	@Override
	public int compareTo(Fruit fruits) {
		// TODO Auto-generated method stub
		if(price<fruits.price)
			return -1;
		else if(price>fruits.price)
			return 1;
		else
			return 0;
	}

	
	
	}



